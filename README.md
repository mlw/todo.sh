# todo.sh

Script to manage a todo-list.

## Install

If you want to install to ~/.local/bin:
`make userinstall`

Or to /usr/local/bin:
`make install`

Uninstalling:
`make (user)uninstall`

## Usage

`todo help`
