NAME=todo
# change this to doas if you use that
SU=sudo

all: userinstall

userinstall:
	cp $(NAME) ~/.local/bin/$(NAME)

useruninstall:
	rm ~/.local/bin/$(NAME)

install:
	$(SU) cp $(NAME) /usr/local/bin/$(NAME)

uninstall:
	$(SU) rm /usr/local/bin/$(NAME)
